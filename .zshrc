ulimit -S -n 20480

export ZSH="/Users/maxkorp/.oh-my-zsh"

ZSH_THEME="robbyrussell"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

source $HOME/.bin/cowhand


export VENV_ROOT_DIR=$HOME/.venv

export NVM_DIR="$HOME/.nvm"
source $(brew --prefix nvm)/nvm.sh --no-use


export PATH=${GOPATH//://bin:}/bin:$HOME/.go/bin:$PATH:$GOPATH/bin
export PATH=$HOME/.bin:$PATH
export _PATH=$PATH

function load_nvmrc {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}

alias land="goland"

findGopath () {
  cdir=$PWD
  while [ "$cdir" != "/" ]; do
    if [ -e "$cdir/.gopath" ]; then
      if [ "$cdir" == "$HOME" ]; then
        _locality="global"
      else
        _locality="local"
      fi

      _gopath=$(cat $cdir/.gopath)
      if [ -n "$_gopath" ]; then
        _type="explicitly set"
      else
        _type="inferred"
        _gopath=$cdir
      fi

      if [ "$_locality" == "global" ] && [ "$_type" == "inferred" ]; then
        _gopath=""
      fi

      _found="1"

      break
    fi
    cdir=$(dirname "$cdir")
  done

  if [ -z "$_found" ] && [ -e "$HOME/.gopath" ]; then
    _gopath=$(cat $HOME/.gopath)
    _locality="global"
    if [ -n "$_gopath" ]; then
      _type="explicitly set"
    else
      _type="inferred"
      _gopath=$HOME
    fi

    if [ "$_locality" == "global" ] && [ "$_type" == "inferred" ]; then
        _gopath=""
      fi

      _found="1"
  fi
  if [ "$_gopath" != "$GOPATH" ]; then
    if [ -n "_gopath" ]; then
      export GOPATH=$_gopath
      export PATH=$_PATH:$GOPATH/bin
      echo "Found $_locality .gopath, exporting $_type $_gopath"
    else
      echo "no .gopath found, unsetting GOPATH"
      unset GOPATH
      export PATH=$_PATH
    fi
  fi
  unset cdir
  unset _found
  unset _gopath
  unset _type
  unset _locality
}


findVenv () {
  cdir=$PWD
  while [ "$cdir" != "/" ]; do
    if [ "$cdir" != "$HOME" ] && [ -e "$cdir/.venv" ]; then
      _venv=$(cat "$cdir/.venv")
      if [ -n "$_venv" ]; then
        _found="1"
        if [ -n "$VIRTUAL_ENV" ]; then
          _current=$(basename $VIRTUAL_ENV)
        fi

        if [ "$_current" != "$_venv" ]; then
          source "$VENV_ROOT_DIR/$_venv/bin/activate"
          echo ".venv file found, activating environment \"$_venv\""
        fi
      fi
      break
    fi

    cdir=$(dirname "$cdir")
  done

  if [ -z "$_found" ] && [ "$(type -w deactivate)" == "deactivate: function" ]; then
    echo "no .venv found, deactivating"
    deactivate
  fi
  unset cdir
  unset _found
  unset _venv
  unset _current
}

cd () {
  builtin cd "$@"
  findGopath
  findVenv
  load_nvmrc
}

cd .

function set_kube_config {
  if [ -z $1 ]
  then 
    export KUBECONFIG=""
  else 
    export KUBECONFIG=$(pwd)/$1
  fi
}
alias skc=set_kube_config

alias gml="gometalinter --config=/users/maxkorp/.gometalinter.json app/... pkg/... server/..."








export PATH=~/.bin:$PATH
ZSH_CUSTOM=~/.zsh_custom
export ZSH=~/.oh-my-zsh

ZSH_THEME="spaceship"
plugins=(git mix)
source $ZSH/oh-my-zsh.sh

export NVM_DIR="/Users/maxkorp/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc

export UPDATE_ZSH_DAYS=1
COMPLETION_WAITING_DOTS="true"

if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nano'
else
  export EDITOR='atom --wait'
fi

export EDITOR='nano'

alias npmr="npm run --silent"

alias serve="http-serve -c-1"

alias gk="gitkraken"

alias lsl="ls -al"

alias y="yarn"

alias yf="yarn flow"

alias yl="yarn lint --cache"

alias yi="yarn add"

alias ygi="yarn global add"

alias yid="yarn add --dev"

alias yw="yarn web"

alias ys="yarn start"

alias yp="yarn prepush"

alias yc="yarn clean"

alias yt="yarn format"

alias yd="yarn docker"

alias z="source $HOME/.zshrc"

alias cfg='/usr/local/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

export WOLFRAM_ALPHA_KEY="56X49E-K79T3R8R8T"

export SPACESHIP_PROMPT_ORDER=(
  time          # Time stampts section
  user          # Username section
  host          # Hostname section
  dir           # Current directory section
  git           # Git section (git_branch + git_status)
  node          # Node.js section
  ruby          # Ruby section
  elixir        # Elixir section
  pyenv         # Pyenv section
  exec_time     # Execution time
  line_sep      # Line break
  exit_code     # Exit code section
  char          # Prompt character
)

export SPACESHIP_GIT_STATUS_SHOW=false;

source "/Users/maxkorp/.zsh_custom/themes/spaceship.zsh-theme"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/maxkorp/.bin/google-cloud-sdk/path.zsh.inc' ]; then source '/Users/maxkorp/.bin/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/maxkorp/.bin/google-cloud-sdk/completion.zsh.inc' ]; then source '/Users/maxkorp/.bin/google-cloud-sdk/completion.zsh.inc'; fi

export HUSKY_SKIP_INSTALL=true

if env | grep -q ^ZSHRC_RESOURCE=
then
  echo "sourced zshrc"
else
  export ZSHRC_RESOURCE="true"
fi
