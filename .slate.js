const isWideMon = (rect) => {
  return (rect.width === 3008 && rect.height === 1692);
};

let screens = {};
const buildScreens = () => {
  screens = {};
  slate.eachScreen((screen) => {
    const rect = screen.rect();
    rect.id = screen.id();
    if (isWideMon(rect)) {
      screens.wide = rect;
    }
    else {
      screens.laptop = rect;
    }
  });
  screens.laptop = screens.laptop || screens.wide;
  screens.wide = screens.wide || screens.laptop;
};
buildScreens();

const wideLeft = (win) => {
  win.doOperation(slate.operation('move', {
    x: screens.wide.x,
    y: screens.wide.y,
    width: screens.wide.width / 4,
    height: screens.wide.height,
    screen: screens.wide.id
  }));
};

const secondaryLeft = (win) => {
  if (screens.wide) {
    return wideLeft(win);
  }

  return laptopFullSize(win);
};

const wideRight = (win) => {
  win.doOperation(slate.operation('move', {
    x: screens.wide.x + (screens.wide.width / 4),
    y: screens.wide.y,
    width: screens.wide.width * 3 / 4,
    height: screens.wide.height,
    screen: screens.wide.id
  }));
};

const secondaryRight = (win) => {
  if (screens.wide) {
    return wideRight(win);
  }

  return laptopFullSize(win);
}

const laptopFullSize = (win) => {
  const name = win.app().name();
  let y = screens.laptop.y;
  let height = screens.laptop.height;
  if (name === 'KiCad') {
    y += 22;
    height -= 22;
  }
  win.doOperation(slate.operation('move', {
    x: screens.laptop.x,
    y: y,
    width: screens.laptop.width,
    height: height,
    screen: screens.laptop.id
  }));


  // win.doOperation(slate.operation('move', screens.laptop));
};

const normalize = () => {
  buildScreens();
  const windows = [];
  slate.eachApp((app) => {
    app.eachWindow((win) => {
      windows.push(win);
    });
  });

  windows.forEach((win) => {
    if (screens.laptop == screens.wide) {
      return laptopFullSize(win);
    }

    const name = win.app().name();

    if (name === 'iTerm2') {
      secondaryLeft(win);
    }
    else if (['Atom', 'Code', 'GoLand'].includes(name)) {
      secondaryRight(win);
    }
    else {
      laptopFullSize(win);
    }
  });
};

slate.bind('1:ctrl', normalize);
